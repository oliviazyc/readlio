Rails.application.routes.draw do

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get    'unlock_account'       => 'users#unlock_account'

  # Dynamic error pages
  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  # devise_for :users
  # Static pages
  root                      'static_pages#home'
  # get    'terms'         => 'static_pages#terms'
  # get	   'privacy'       => 'static_pages#privacy'
  # get    'about'         => 'static_pages#about'
  # get    'apps'          => 'static_pages#apps'
  # get    'changelog'     => 'static_pages#changelog'
  # get    'feedback'      => 'static_pages#feedback'
  get    'support'       => 'static_pages#support'

  # Sessions
  get	   'login'		     => 'sessions#new'
  post	 'login'		     => 'sessions#create'
  delete 'logout'	       => 'sessions#destroy'

  # Users
  # get    'profile'       => 'users#show'
  get    'signup'        => 'users#new'
  
  resources :users, only: [:new, :show, :create, :edit, :update, :destroy]
  
  # Destroy
  get  'users/:id/remove_all_books' => 'users#remove_all_books', as: :remove_all_books
  get  'users/:id/remove_all_classbooks' => 'users#remove_all_classbooks', as: :remove_all_classbooks
  get  'users/:id/remove_all_lists' => 'users#remove_all_lists', as: :remove_all_lists
  
  # Lists
  get    'import_page'        => 'lists#import_page'
  
  resources :lists
  
  # Books
  get    'search'        => 'books#search'
  
  resources :books
  resources :reviews, only: [:edit, :update]

  # Password Resets
  resources :password_resets, only: [:new, :create, :edit, :update]
  get 'password_resets/new'
  get 'password_resets/edit'

  # Classrooms
  get    'classroom_join'      => 'classrooms#join'
  get    'classroom_activity'  => 'classrooms#activity'
  get    'student'             => 'classrooms#student'
  get    'leaderboards'        => 'classrooms#leaderboards'
  get    'students'            => 'classrooms#students'
  get    'archived'            => 'classrooms#archived'
  get    'info'                => 'classrooms#info'
  get    'leave_classroom'     => 'classrooms#leave'
  
  get  'classrooms/:id/remove' => 'classrooms#remove_student', as: :remove_student
  get  'classrooms/:id/remove_self' => 'classrooms#remove_self', as: :remove_self
  get  'classrooms/:id/archive' => 'classrooms#archive', as: :archive
  
  resources :classrooms
  resources :terms
  
  # Classbooks
  get    'classroomsearch'     => 'classbooks#search'
  get    'classbooks_print'    => 'classbooks#print'
  resources :classbooks
  
end