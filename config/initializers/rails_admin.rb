RailsAdmin.config do |config|

  config.authenticate_with do |controller|
    unless current_user && current_user.admin?
      redirect_to(main_app.root_path)
    end
  end

  config.parent_controller = "::ApplicationController"

  config.configure_with(:import) do |config|
    config.logging = true
    config.line_item_limit = 1000
    config.update_if_exists = false
    config.rollback_on_error = false
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show

    # Rails admin import gem
    all
    import
  end
end
