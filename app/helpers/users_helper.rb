module UsersHelper
  
  def avatar_for(user, options = { size: 80 })
    size = options[:size]
    if user.avatar?
      image_tag user.avatar.url, class: "avatar", height: size, width: size
    else
      gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
      size = options[:size]
      gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=mm&s=#{size}"
      image_tag(gravatar_url, alt: user.name, class: "avatar", height: size, width: size)
    end
  end
end
