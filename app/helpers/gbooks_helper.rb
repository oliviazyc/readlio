module GbooksHelper
    
    def g_item_search(keywords)
        # results = GoogleBooks.search("The Great Gatsby", {:count => 10, :api_key => 'AIzaSyCVTB9iOjFmXUPD0zmUjGF-RTfTqfscor4'}) 
        response = GoogleBooks.search(keywords, {:count => 10}) 
        @results = []
        response.each do |item|
            result = {}
            result[:isbn] = item.isbn.to_s
            result[:url] = item.info_link
            # Title, pages
            result[:title] = item.title
            result[:pages] = item.page_count
            
            result[:image_url] = item.image_link(zoom: 3)
            
            # Author
            result[:author] = item.authors
            
            # Editorial Review
            result[:editorial_review] = item.description
            @results << result
        end
    end
    
end