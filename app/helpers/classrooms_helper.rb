module ClassroomsHelper

  def total_pages(array_of_books)
    return array_of_books.sum(:pages)
	end

	def find_classbooks(classroom_id, user_id, term_id, attributes="", associations="")
		error if classroom_id.nil?
		
		filters = {}
		filters[:classroom_id] = classroom_id
		filters[:user_id] = user_id if user_id
		filters[:term_id] = term_id if term_id

	    if associations.empty?
	  		unless attributes.empty?
	  			classbooks = Classbook.where(filters).select(attributes)
	  		else
	  			classbooks = Classbook.where(filters)
	  		end
	    else
	      unless attributes.empty?
	  			classbooks = Classbook.where(filters).select(attributes).includes(associations)
	  		else
	  			classbooks = Classbook.where(filters).includes(associations)
	  		end
	    end

		return classbooks
	end

	def return_sort(user, sort)
	  if sort == "Last Name"
			return user.name.split(' ').last.downcase
		  elsif sort == "Number of Books"
			return -find_classbooks(@classroom.id, user.id, @selected_term_id).count # Negative because we want those with most books to appear at the top
		  elsif sort == "Number of Pages"
		  return -total_pages(find_classbooks(@classroom.id, user.id, @selected_term_id)) # Same goes for pages
		else
			return user.name.downcase # by default, sort by first name
		end
	end

	def get_current_term(classroom)
	# Find term by current term id of classroom, return nil if not found
		if classroom
			return Term.find_by(id: classroom.current_term_id)
		else
			return nil
		end
	end

	def classroom_role(classroom)
		classroom_relationship = ClassroomStudent.where(user_id: current_user.id, classroom_id: classroom.id).select("classroom_students.id").first
		return "Student" if classroom_relationship && !classroom_relationship.nil? 
		classroom_relationship = ClassroomTeacher.where(user_id: current_user.id, classroom_id: classroom.id).select("classroom_teachers.id").first
		return "Teacher" if classroom_relationship && !classroom_relationship.nil?
		return nil
	end

	def fancy_string_teachers(classroom)
		# Find teachers
		teachers = "" # Make empty string
		if classroom.teachers.count == 1 # If there's only one teacher
			teachers = classroom.teachers.first.name # Then set the teachers to the one teacher
		elsif classroom.teachers.count > 1 # Or if it's greater than one
			classroom.teachers.each do |teacher| # Go through each teacher
				if teacher == classroom.teachers.last # If it is last, we don't want a comma in back
					teachers = teachers + teacher.name # Put the teacher name in the string
			  else
				  teachers = teachers + teacher.name + ", " # Put comma in back (since it's not the last one)
			  end
			end
		end

		return teachers
	end

end
