class ReviewsController < ApplicationController
    before_action :logged_in_user
	before_action :correct_user,   only: [:edit, :update]

	def edit
	    # @review = Review.find(params[:id])
	end
    def update
        # @review = Review.find(params[:id])
	    if @review.update_attributes(review_params)
	    	flash[:success] = "Your review was saved."
	      if @review.model_type == "Book"
		      book = Book.find(@review.book_id)
		      redirect_to book_path(book)
		  else
		  	  classbook = Classbook.find(@review.book_id)
		      redirect_to classbook_path(classbook)
		  end
	    else
	      render 'edit'
	    end
	end

	private

	  def review_params
			params.require(:review).permit(:asin, :user_id, :book_id, :content, :public)
		end

		def correct_user
			@review = current_user.reviews.find_by(id: params[:id])
			if !@review.nil? || current_user.admin
				if @review.model_type == "Book"
					@book = current_user.books.find_by(id: @review.book_id) # Instance variables used here because then they are passed on to edit and update
				else
					@book = current_user.classbooks.find_by(id: @review.book_id)
				end
			else
				redirect_to classrooms_path
			end
		end

end
