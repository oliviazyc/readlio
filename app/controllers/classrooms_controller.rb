class ClassroomsController < ApplicationController
	# before_action :authenticate_user!
	before_action :logged_in_user
	before_action :correct_user,   only: [:show, :edit, :update, :destroy]
	before_action :find_classroom, except: [:index, :archived, :new, :join, :create]
	before_action :get_classroom_role, except: [:index, :archived, :new, :join, :create]
	before_action :user_belongs_to_classroom, except: [:index, :archived, :new, :join, :create]
	before_action :get_teachers, except: [:index, :archived, :new, :join, :create]
	# before_action :check_if_student_exists_in_classroom, only: [:student]
	before_action :setup_markdown, only: [:student, :show, :activity]
	before_action :teacher_only, only: [:student, :edit, :update, :destroy, :remove_student, :archive, :students, :info]
	before_action :check_if_current_user_is_teacher, only: [:remove_teacher]

	def index
		classrooms = current_user.classrooms("classrooms.id, classrooms.title, classrooms.archived")
		@classrooms = []
		@archived_classrooms = false

		classrooms.each do |classroom|
			unless classroom.archived
				@classrooms << classroom
			else
				@archived_classrooms = true
			end
		end
		# unless cookies[:msg_shown]
		# 	flash[:success] = "Due to an unexpected amount of traffic, our site has encountered a few bugs. We are actively working on a solution - thanks for your support!"
		# 	cookies[:msg_shown] = true
		# end
	end

	def new
		@classroom = current_user.teaching_classrooms.build if logged_in?
		# 1.times {@classroom.terms.build}
	end

	def create
		@classroom = current_user.teaching_classrooms.create(classroom_params)
		if @classroom.save
			puts "CLASSROOM WORKED"
			flash[:success] = "Your new classroom, "+ @classroom.title + ", was created."
			redirect_to classroom_path(@classroom)
		else
			render 'classrooms/new'
			puts "CLASSROOM DIDNT WORK"
			# 1.times {@classroom.terms.build} # Doesn't work
		end
	end

	def show
		# Determine @selected_term_id
		if @classroom.has_terms?
			if params[:term_id]
				@selected_term_id = params[:term_id]
				if params[:term_id] == ''
					@selected_term_id = nil
				end
			else
				if @classroom.has_current_term?
					@selected_term_id = @classroom.current_term_id
				else
					@selected_term_id = nil
				end
			end
		else
			@selected_term_id = nil # Can this be taken out?
		end
		# do you really need to have .select when you're mostly loading all of the classbook already?
		@current_user_current_classbooks = find_classbooks(@classroom.id, current_user.id, @selected_term_id, "
																												classbooks.id,
																												classbooks.user_id,
																												classbooks.title,
																												classbooks.author,
																												classbooks.created_at,
																												classbooks.pages,
																												classbooks.manual,
																												classbooks.image_url", "user")

		@current_user_total_pages = total_pages(@current_user_current_classbooks)

		@current_term = get_current_term(@classroom)
	end

	def student
		@student = User.find(params[:student_id])

		if @classroom.has_terms?
			if params[:term_id]
				@selected_term_id = params[:term_id]
				if params[:term_id] == ''
					@selected_term_id = nil
				end
			else
				if @classroom.has_current_term?
					@selected_term_id = @classroom.current_term_id
				end
			end
		end
		@classbooks = find_classbooks(@classroom.id, @student.id, @selected_term_id)

		unless @student.nil?
			@student_total_pages = total_pages(@classbooks)
		end

	end

	def activity
		# Find all classroom classbooks
		# @classroom = Classroom.where(id: params[:classroom_id]).select("classrooms.id, classrooms.title, classrooms.required_pages, classrooms.archived").first
		@classbooks = Classbook.where(classroom_id: @classroom.id).paginate(:page => params[:page], :per_page => 10).includes(:user).select("classbooks.id,
																																			classbooks.title,
																																			classbooks.author,
																																			classbooks.user_id,
																																			classbooks.created_at,
																																			classbooks.pages,
																																			classbooks.review,
																																			classbooks.manual,
																																			classbooks.image_url,
																																			classbooks.amazon_url")
	end

	def join
		if params[:classcode]
			classcode = Classcode.find_by(value: params[:classcode])
			@classroom = Classroom.find_by(id: classcode.classroom_id) if classcode

			unless @classroom.nil?
				# Check if the user is already a student
				classroom_relationship = ClassroomStudent.find_by(user_id: current_user.id, classroom_id: @classroom.id)
				# Check if a user is already a teacher if student is not found
				classroom_relationship = ClassroomTeacher.find_by(user_id: current_user.id, classroom_id: @classroom.id) if classroom_relationship.nil?

				if classroom_relationship.nil?
					if classcode.relationship_type == "Student"
						@classroom.students << current_user
					elsif classcode.relationship_type == "Teacher"
						@classroom.teachers << current_user
					end

					flash[:success] = "You successfully joined " + @classroom.title + "."
					redirect_to classroom_path(@classroom)
				else
					flash[:danger] = "You're already in this class."
				end
			else
				flash[:danger] = "We couldn't find any classes with that class code. Are you sure it's correct?"
			end
		end
	end

	def leaderboards
		@selected_term_id = @classroom.current_term_id
	end

	def info
		@classbooks = Classbook.where(classroom_id: @classroom.id).select("classbooks.id")
		@classroom_total_pages = total_pages(@classbooks)
		@current_term = get_current_term(@classroom)
	end

	def update
	    if @classroom.update_attributes(classroom_params)
	      flash[:success] = "Your changes were saved."
	      redirect_to edit_classroom_path(@classroom)
	    else
	      render 'edit'
	    end
	end

	def destroy
		flash[:success] = "Your classroom was deleted."
		redirect_to classrooms_path
		@classroom.destroy
	end

	def remove_student # For teachers removing students
		# Find classroom relationship, student, and classroom
		@classroom_student = ClassroomStudent.find_by_user_id_and_classroom_id(params[:id], params[:classroom_id])
		@student = User.find(params[:id])
		@classroom = Classroom.find(params[:classroom_id])

		# Remove classbooks
		@student.classbooks.each do |classbook|
			if classbook.classroom_id == @classroom.id
				classbook.destroy
			end
		end

		# Show success message and redirect
		flash[:success] = @student.name + " was removed from " + @classroom.title + "."
		redirect_to classroom_path(@classroom)

		# Delete
		@classroom_student.destroy
	end

	def remove_self # For student removing self or teacher removing self
		@classroom_relationship = ClassroomStudent.find_by(user_id: current_user.id, classroom_id: params[:id])
		@classroom_relationship = ClassroomTeacher.find_by(user_id: current_user.id, classroom_id: params[:id]) if @classroom_relationship.nil?

		@classroom = Classroom.find(params[:id])

		# Remove classbooks (only applicable to student)
		current_user.classbooks.each do |classbook|
			if classbook.classroom_id == @classroom.id
				classbook.destroy
			end
		end

		# Redirect and flash message
		flash[:success] = "You left " + @classroom.title + "."
		redirect_to classrooms_path

		# Delete
		@classroom_relationship.destroy
	end

	# Action link for archiving classrooms
	def archive
		@classroom = current_user.teaching_classrooms.find_by(id: params[:classroom_id])
		@classroom.toggle(:archived)
		@classroom.save
		if @classroom.archived
			flash[:success] = "Successfully archived " + @classroom.title + "."
		else
			flash[:success] = "Successfully unarchived " + @classroom.title + "."
		end
		redirect_to classrooms_path
	end

	# Page of archived classrooms
	def archived
		@classrooms = current_user.classrooms.select { |classroom| classroom.archived == true }
	end

	private

		def find_classroom
			if params[:classroom_id]
				@classroom = Classroom.find(params[:classroom_id])
			elsif params[:id]
				@classroom = Classroom.find(params[:id])
			end
		end

		def classroom_params
			params.require(:classroom).permit(:title, :required_pages, :user_id, :activity_feed_enabled, :leaderboards_enabled, :judge_leaderboards_by, terms_attributes: [:id, :title, :start_date, :end_date, :_destroy])
		end

		def correct_user
			@classroom = current_user.teaching_classrooms.find_by(id: params[:id])
			@classroom = current_user.attending_classrooms.find_by(id: params[:id]) if @classroom.nil?
			# @classroom = Classroom.find_by(id: params[:id]) if @classroom.nil?
			redirect_to classrooms_path if @classroom.nil?
		end

		def get_classroom_role
			# Find the current user's role in the classroom: Teacher or Student
			@classroom_role = classroom_role(@classroom)
		end

		def user_belongs_to_classroom
			redirect_to classrooms_path if @classroom_role.nil?
		end

		def teacher_only
			puts "teacher_only"
			unless @classroom_role == "Teacher"
				redirect_to classrooms_path
			end
		end

		def get_teachers 
			@teachers = fancy_string_teachers(@classroom)
		end

		def check_if_student_exists_in_classroom
			classroom_student = ClassroomStudent.find_by(user_id: params[:student_id], classroom_id: @classroom.id)
			redirect_to classrooms_path if classroom_student.nil?
		end

end
