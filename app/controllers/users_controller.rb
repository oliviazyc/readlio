class UsersController < ApplicationController
  # before_action :authenticate_user!, only: [:edit, :update, :admin, :show, :explore]
  before_action :logged_in_change_links, only: [:new]
  before_action :logged_in_user, only: [:edit, :update, :admin]
  before_action :correct_user,   only: [:edit, :update]
  # before_action :profile_private, only: [:show]
  before_action :filter_admin, only: [:admin]
  before_action :authenticate, only: [:admin]
  before_action :check_if_user_exists, only: [:pedit]
  
  def unlock_account
    if params[:email]
      @user = User.find_by(email: params[:email])
      if @user
        log_in @user
        redirect_to pedit_path
        flash.now[:success] = "We found your account."
      else
        flash.now[:danger] = "Couldn't find a user with that email. Check if there are typos."
      end
    end
  end
	
	def new
  	@user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      # Create 3 preset lists when a user signs up.
      @user.lists.create(title: "Already Read")
      @user.lists.create(title: "Want to Read")
      @user.lists.create(title: "Favorites")
      redirect_to classrooms_path
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])

    @user_classbooks = @user.classbooks.select("classbooks.id, classbooks.image_url, classbooks.manual, classbooks.title, classbooks.author, classbooks.pages, classbooks.created_at").limit(10)
    @user_books = @user.books.select("books.id, books.image_url, books.manual, books.title, books.author, books.pages, books.created_at").limit(10)
    @recent_books = (@user_classbooks + @user_books).sort_by &:created_at
    @books_count =  @user.classbooks.count + @user.books.count
    @page_count = @user.classbooks.sum(:pages) + @user.books.sum(:pages)
    
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Your settings were saved."
      redirect_to edit_user_path(@user)
    else
      flash.now[:danger] = "An error occurred with your submission."
      render 'edit'
    end
  end

  def destroy
    @user = User.find_by(id: params[:id])
    
    # Destroy all of the user's lists
    @user.lists.each do |list|
      list.destroy
    end

    # Destroy all of the user's classbooks
    @user.classbooks.each do |classbook| 
      classbook.destroy
    end
    
    # Destroy all of the user's classrooms
    @user.teaching_classrooms.each do |classroom| 
      classroom.destroy
    end
    
    @user.destroy
    flash[:success] = "Your account was deleted."
    redirect_to signup_path
  end
  
  def admin
    @teachers = []
    @students = []
    User.all.each do |user|
      @teachers << user if user.teaching_classrooms.any?
      @students << user if user.attending_classrooms.any?
    end
  end
  
  def remove_all_books
    current_user.books.each do |book|
      book.destroy
    end
    flash[:success] = "Your books inside lists were successfully deleted."
    redirect_to edit_user_path(current_user)
  end
  
  def remove_all_classbooks
    current_user.classbooks.each do |classbook|
      classbook.destroy
    end
    flash[:success] = "Your books inside classrooms were successfully deleted."
    redirect_to edit_user_path(current_user)
  end
  
  def remove_all_lists
    current_user.lists.each do |list|
      list.destroy
    end
    flash[:success] = "Your lists were successfully deleted."
    redirect_to edit_user_path(current_user)
  end

  private
    def filter_admin
      unless current_user.admin == true
        raise_error(ActionController::RoutingError)
      end
    end

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :avatar, :remove_avatar, :bio, :lists_enabled, :profile_private)
    end

    # Before filters

    # Signup => Profile if logged in
    def logged_in_change_links
      unless not logged_in?
        redirect_to classrooms_path
      end
    end

    def authenticate
      authenticate_or_request_with_http_basic('Administration') do |code|
        code == 'best29cacti'
      end
    end
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    def profile_private
      @user = User.find(params[:id])
      unless @user == current_user
        redirect_to(root_url) if @user.profile_private
      end
    end

    def check_if_user_exists
      unless logged_in?
        redirect_to root_url
      end
    end


end