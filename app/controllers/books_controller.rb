class BooksController < ApplicationController
	include GbooksHelper

	# before_action :authenticate_user!
	# Before filters
	before_action :logged_in_user # Need to be logged in to perform any of these actions
	before_action :correct_user,   only: [:show, :edit, :update, :destroy] # Need to own the book to see it, edit/update it, or destroy it
	before_action :manual_only, only: [:edit] # If the book isn't manual, then there is a reviews#edit page for the review. Otherwise, the review was stored on the book.
	before_action :get_list, only: [:new, :search]

	def new
		get_list_options # Get list options for select_tag
		new_book_instance # Make a new empty book
		
	end # end new

	def search
		get_list_options # Get list options for select tag
		new_book_instance # Make a new empty book
		if params[:keywords]
			g_item_search(params[:keywords])
		end
	end

	def index # books index is really a redirect to lists_path
		redirect_to lists_path
	end

	def create
		get_list_options # This was causing the options for select error. Know that Rails needs to know the options for select in create as well.
		# begin
			# Find list to add from the select tag
			list = current_user.lists.find(book_params[:list_id])
			# Create book from book params
			@book = list.books.build(book_params)
	  		 # Success
			if @book.save
				# If the student wrote a review (works even if it was blank)
				# Review.create(asin: @book.asin, content: params[:review], user_id: current_user.id, book_id: @book.id, model_type: "Book") # Create a new Review with the content of that review
				flash[:success] = @book.title + " was added to " + list.title + "." # Success message.
				redirect_to list_path(list) # Go to list_path with the book.
			 # Fail
			else
				render 'books/new', params: params
				# If it's manual, than it likely failed because the user didn't enter a title.
				# If it failed and it wasn't manual, then Amazon probably didn't supply a title.
			end
		# rescue
		# 	flash[:danger] = "Something went wrong with your request."
		# 	redirect_to request.referer
		# end
	end

	def show
		get_book # Retrieve the book that is being shown.
		setup_markdown # Setup markdown parser - for the editorial review and users review.
		@list = current_user.lists.find(@book.list_id)
	end

	def update
		get_book
	    if @book.update_attributes(book_params)
	      flash[:success] = "Your review was saved."
	      redirect_to book_path(@book)
	    else
	      render 'edit'
	    end
	end

	def destroy
		list = current_user.lists.find(@book.list_id)
		flash[:success] = @book.title + " was removed from " + list.title + "."
		redirect_to list_path(list.id)
		@book.destroy
	end

	private
		def get_list_options
			@list_options = []
			unless params[:list_id] == nil || params[:list_id].empty?
				current_list = current_user.lists.find(params[:list_id])
				if current_list.user_id == current_user.id
					@list_options.push([current_list.title, current_list.id])
				end
				current_user.lists.each do |list|
					unless list == current_list
						@list_options.push([list.title, list.id])
					end
				end

			else
				current_user.lists.each do |list|
					@list_options.push([list.title, list.id])
				end
			end
		end

		def new_book_instance
			@book = current_user.books.build if logged_in?
		end

		def get_book
			@book = current_user.books.find(params[:id])
		end

		def get_list
			@list = current_user.lists.find(params[:list_id])
		end

		def book_params
			params.require(:book).permit(:title, :author, :list_id, :asin, :amazon_url, :image_url, :pages, :manual, :review, :description, :isbn)
		end

		def correct_user
			@book = current_user.books.find_by(id: params[:id])
			if @book.nil?
				redirect_to lists_path
			end
		end

		def manual_only
			@book = current_user.books.find(params[:id])
			unless @book.manual
				review = Review.find_by(book_id: @book.id)
				redirect_to edit_review_path(review) if review
			end
		end
end
