class StaticPagesController < ApplicationController
	# before_action :set_footer
	before_action :logged_in_change_links, only: [:home]

	def home
		@home = true
	end

	# Home => Profile if logged in
	def logged_in_change_links
	  if logged_in?
	    redirect_to classrooms_path
	  end
	end

end
