class SessionsController < ApplicationController
  before_action :logged_in_change_links, only: [:new]
  
  def new
    @login_signup == true
  end

  def create
  	user = User.find_by(email:params[:session][:email].downcase)
  	if user && user.authenticate(params[:session][:password])
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      redirect_destination = classrooms_path
      redirect_back_or redirect_destination
    else
      # Create an error message.
      flash.now[:danger] = 'Your email or password is incorrect.'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

  # Before Filters
  # Login => Profile if logged in
  def logged_in_change_links
    if logged_in?
      redirect_to classrooms_path
    end
  end

end
