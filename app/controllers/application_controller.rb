class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  include ClassroomsHelper
  before_filter :prepare_exception_notifier
  
  # before_action :configure_permitted_parameters, if: :devise_controller?

  # protected

  # def configure_permitted_parameters
  # devise_parameter_sanitizer.for(:sign_in)        << :name
  # end

  private

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in? && (not current_user == nil)
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    def setup_markdown
      # Use RedCarpet to initialize a markdown reader.
      @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: true)
    end
    
    def prepare_exception_notifier
      request.env["exception_notifier.exception_data"] = {
        :current_user => current_user
      }
    end
		
end
