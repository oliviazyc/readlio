class PasswordResetsController < ApplicationController
  before_action :get_user, only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]
  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = "An email was sent with password reset instructions to " + @user.email + "."
      redirect_to login_path
    else
      flash[:danger] = "We don't recognize that email. Are you sure that's the one you signed up with?"
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:user][:password].empty?
      flash[:danger] = "Your new password can't be empty."
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      flash[:success] = "Your password has been reset."
      redirect_to classrooms_path
    else
      render 'edit'

    end
  end

  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end
  
  def get_user
    @user = User.find_by(email: params[:email])
  end

  # Checks expiration of reset token
  def check_expiration
    if @user && @user.password_reset_expired?
      flash[:danger] = "Your password reset has expired."
      redirect_to new_password_reset_url
    elsif @user.nil?
      flash[:danger] = "You seem to be missing one or more parameters."
      redirect_to new_password_reset_url
    end
  end
  
end
