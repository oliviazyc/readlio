class ClassbooksController < ApplicationController
	include GbooksHelper

	# before_action :authenticate_user!
	before_action :logged_in_user
	before_action :new_classbook_instance, only: [:new, :search]
	before_action :find_classroom, only: [:new, :search]
	before_action :options_for_select, only: [:new, :search, :create]
	before_action :correct_user, only: [:show, :edit, :update, :destroy]
	before_action :user_belongs_to_classroom, only: [:new, :search, :create]
	before_action :correct_user_by_user_id, only: [:index]

	def new
		@classroom = Classroom.where(id: params[:classroom_id]).select("classrooms.id, classrooms.title, classrooms.current_term_id, classrooms.archived, classrooms.required_pages").first
		@current_term = get_current_term(@classroom)
	end

	def search
		@current_term = get_current_term(@classroom) if @classroom
		setup_markdown
		if params[:keywords]
			g_item_search(params[:keywords]) # stopped using amazon
		end
	end

	def create
		classroom = Classroom.find_by(id: classbook_params[:classroom_id])
		classbook_params[:term_id] = classroom.current_term_id if classroom
		classroom_role = classroom_role(classroom)
		@classbook = current_user.classbooks.build(classbook_params)
		if classroom && classroom_role && @classbook.save
			flash[:success] = @classbook.title + " was added."
			redirect_to classroom_path(classroom.id)
		else # Fail
			if !classroom_role
				flash[:danger] = "You don't have access to that classroom."
			end
			render 'classbooks/new'
		end

	end

	def index
		# @user = User.find(params[:user_id])
		@classroom = Classroom.find(params[:classroom_id])
		redirect_to classroom_path(@classroom)
	end

	def show
		setup_markdown

		# Find classbook and classroom
		@classbook = current_user.classbooks.find(params[:id])
		@classroom = Classroom.where(id: @classbook.classroom_id).select("classrooms.id, classrooms.title, classrooms.current_term_id, classrooms.archived, classrooms.required_pages").first
		@current_term = Term.find_by(id: @classroom.current_term_id)
		@term = Term.find_by(id: @classbook.term_id)
	end

	def edit
		@classbook = Classbook.find(params[:id])
		@classroom = Classroom.where(id: @classbook.classroom_id).select("classrooms.id, classrooms.title, classrooms.current_term_id, classrooms.archived, classrooms.required_pages").first
	end

	def update
	    if @classbook.update_attributes(classbook_params)
	      flash[:success] = "Your changes were saved."
	      redirect_to classbook_path(@classbook)
	    else
	      render 'edit'
	    end
	end

	def destroy
		classroom = Classroom.find(@classbook.classroom_id)
		flash[:success] = "Your book was removed."
		redirect_to classroom_path(classroom)
		@classbook.destroy
	end

	def print
		@classroom = Classroom.find_by(id: params[:classroom_id])

		if @classroom.has_terms?
			if params[:term_id]
				@selected_term_id = params[:term_id]
				if params[:term_id] == ''
					@selected_term_id = nil
				end
			else
				if @classroom.has_current_term?
					@selected_term_id = @classroom.current_term_id
				else
					@selected_term_id = nil
				end
			end
		else
			@selected_term_id = nil # Can this be taken out?
		end

		@classbooks = find_classbooks(@classroom.id, current_user.id, @selected_term_id)

		@total_pages = total_pages(@classbooks)
	end

	private
		def classbook_params
			params.require(:classbook).permit(:title, :author, :asin, :amazon_url, :image_url, :pages, :manual, :review, :description, :classroom_id, :term_id, :isbn)
		end

		def new_classbook_instance
			@classbook = current_user.classbooks.build if logged_in?
		end

		def find_classroom
			if params[:classroom_id]
				@classroom = Classroom.find(params[:classroom_id])
			elsif params[:id]
				@classroom = Classroom.find(params[:id])
			end
		end

		def options_for_select
			@classroom_options = []
			unless params[:classroom_id] == nil || params[:classroom_id].empty?
				current_classroom = Classroom.where(id: params[:classroom_id]).select("classrooms.id, classrooms.title, classrooms.archived").first

        		if current_classroom && !current_classroom.nil? && !current_classroom.archived && !classroom_role(current_classroom).nil?
        			@classroom_options.push([current_classroom.title, current_classroom.id])
				end

				current_user.classrooms("classrooms.id, classrooms.title, classrooms.archived").each do |classroom|
					unless classroom == current_classroom || classroom.archived
						@classroom_options.push([classroom.title, classroom.id])
					end
				end

			else
				current_user.classrooms("classrooms.id, classrooms.title, classrooms.archived").each do |classroom|
					@classroom_options.push([classroom.title, classroom.id]) unless classroom.archived
				end
			end
		end

		def correct_user
			@classbook = current_user.classbooks.find_by(id: params[:id])
			if @classbook.nil?
				redirect_to classrooms_path
			end
		end

		def user_belongs_to_classroom
			if @classroom
				redirect_to classrooms_path if classroom_role(@classroom).nil?
			end
		end

		def correct_user_by_user_id
			@user = User.find_by(id:params[:user_id])
			unless current_user == @user
				redirect_to classrooms_path
				flash.now[:danger] = "You do not have access to this page."
			end
		end


end
