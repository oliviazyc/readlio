class ListsController < ApplicationController
	# before_action :authenticate_user!
	before_action :logged_in_user
	before_action :get_user
	before_action :correct_user,   only: [:show, :destroy, :edit, :update]
	before_action :get_list, only: [:edit, :update, :show]
	before_action :redirect_to_classrooms_if_no_books
	# before_action :redirect, only: [:update]

	def new
		@list = current_user.lists.build if logged_in?
	end

	def create
		@list = current_user.lists.build(list_params)
		if @list.save
			flash[:success] = "Your new list, "+ @list.title + ", was created."
			redirect_to list_path(@list)
		else
			render 'new'
		end
	end

	def update
	    if @list.update_attributes(list_params)
	      flash[:success] = "Your changes to "+ @list.title + " were saved."
	    	redirect_to edit_list_path(@list)
	    else
	    	render 'edit'
	    end
	end

	def show
		# Calculate total params
		@total_pages = total_pages(@list.books)
	end

	def destroy
		redirect_to lists_path
		flash[:success] = "Your list was deleted."
		@list.destroy
	end

	private
		
		def list_params
			params.require(:list).permit(:title, :user_id, :goodreads)
		end
		
		def get_user
			@user = current_user
		end

		def get_list
			@list = current_user.lists.find(params[:id])
		end

		def correct_user
			@list = current_user.lists.find_by(id: params[:id])
			if @list.nil?
				redirect_to lists_path
			end
		end
		
		def redirect_to_classrooms_if_no_books
			unless current_user.lists_enabled
				redirect_to classrooms_path
			end
		end
		
		# def redirect
		# 	redirect_to root_url
		# end
end
