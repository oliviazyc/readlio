class ApplicationMailer < ActionMailer::Base
  default from: "noreply@readlio.com"
  layout 'mailer'
end
