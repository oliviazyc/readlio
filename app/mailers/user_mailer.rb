class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user

    mail to: user.email, subject: "Password reset for your Readlio account"
  end

  def unlock_account(user)
  	@user = user
  	mail to: user.email, subject: "Action needed: Password Reset for Readlio account", from: "readlioinc@gmail.com"
  end
end
