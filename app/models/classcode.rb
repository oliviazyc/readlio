class Classcode < ActiveRecord::Base
    belongs_to :classrooms
    
    after_create :generate_token
    
    validates :value, uniqueness: { case_sensitive: true }
    
    private
        MAX_RETRIES = 1000
    	def generate_token
    	  update_column :value, SecureRandom.hex(4)
    	rescue ActiveRecord::RecordNotUnique => e
    	  @token_attempts = @token_attempts.to_i + 1
    	  retry if @token_attempts < MAX_RETRIES
    	  raise e, "Retries exhausted"
    	end 
end
