class Classbook < ActiveRecord::Base
	default_scope -> { order(created_at: :desc)}
	
	belongs_to :user
	# has_one :review
	
	# Callbacks
	after_save :init
	
	# Validations
	validates :title, presence: true # There must be a title
	validates :author, presence: true # There must be an author
	#validates :review, presence: true, length: { minimum: 50 }
	validates :classroom_id, presence: true
	validates :user_id, presence: true

	validates :pages, length: { maximum: 2147483647 }, numericality: { only_integer: true }

	# Obscenity filter
	# require 'obscenity/active_model'
	# validates :review,  obscenity: { sanitize: true, replacement: :vowels }

	private
		def init
			classroom = Classroom.find_by(id: classroom_id)
		# 	# Set a term 
			update_column :term_id, classroom.current_term_id if classroom
		end
			
		def destroy_children
			unless self.manual || self.asin.empty?
  				review = Review.find_by(user_id: current_user.id, asin: self.asin, book_id: self.id, model_type: "Classbook")
				review.destroy unless review.nil?
  			end
		end
	# def abandoned_book?

	# 	unless @classbook.manual
	# 		@book_info = item_lookup(@classbook.asin)
	# 	end
	# 	@book_info
end
