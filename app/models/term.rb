class Term < ActiveRecord::Base
    
    # default_scope -> { order(created_at: :asc)}
    
    belongs_to :classroom
    
    # after_create :change_strings_to_dates
    
    validates :title, presence: true
    validates :start_date, presence: true
    validates :end_date, presence: true
    
    private
        
        def change_strings_to_dates
            update_column :start_date, Date.strptime(start_date, "%m/%d/%Y")
            update_column :end_date, Date.strptime(end_date, "%m/%d/%Y")
        end
        
end