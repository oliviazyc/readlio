class List < ActiveRecord::Base
  # require 'csv'
  
  # Default scope
  default_scope -> { order(created_at: :asc)}
  
  # Associations
  belongs_to :user
  has_many :books
  
  # Callbacks
  before_destroy :destroy_children

  # Validations
  validates :user_id, presence: true # Lists must belong to a user.
  validates :title, presence: true, length: { maximum: 50 } # There must be a title
  
  private
    def destroy_children
			Book.where(list_id: self.id).each do |book|
				book.destroy
			end
		end
  
end
