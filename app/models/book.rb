class Book < ActiveRecord::Base
  default_scope -> { order(created_at: :desc)} # Books created last come first
  
  # Associations
  belongs_to :list
  # has_one :review
  
  # Validations
  validates :list_id, presence: true
  validates :title, presence: true

  # Callbacks
  # after_create :fix_timestamp
  # after_find :add_amazon_url

  def user
    return User.find_by(id: list.user_id)
  end
  
  # Obscenity filter
# 	require 'obscenity/active_model'
# 	validates :review,  obscenity: { sanitize: true, replacement: :vowels }
  
  
  private
    def destroy_children
			unless self.manual
  			review = Review.find_by(user_id: current_user.id, asin: self.asin, book_id: self.id, model_type: "Book")
  			review.destroy unless review.nil?
  		end
		end
end
