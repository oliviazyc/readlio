class User < ActiveRecord::Base
	has_many :classroom_teachers
	has_many :classroom_students
	has_many :teaching_classrooms, through: :classroom_teachers
	has_many :attending_classrooms, through: :classroom_students
  
	has_many :lists, dependent: :destroy
	has_many :books, through: :lists, dependent: :destroy
	has_many :classbooks
	has_many :reviews
	
	default_scope -> { order(created_at: :asc)}

	attr_accessor :remember_token, :reset_token
	
	# Callbacks
	before_save { email.downcase! }
	# after_save :init
	
	# Make sure name exists and is less than 50
	validates :name,  presence: true, length: { maximum: 50 }

	# VALID_USERNAME_REGEX = /\A\S\w*$\z/
 	# 	validates :username,  length: { maximum: 50 }, uniqueness: { case_sensitive: false }, format: { with: VALID_USERNAME_REGEX }, allow_nil: true

 	# Set Email format with REGEX
 	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
 	# Make sure email exists, is less than 255, fits VALID_EMAIL_REGEX, and is unique
 	validates :email, presence: true, length: { maximum: 255 },
                	  format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
	has_secure_password
	# Make sure the password has a minimum length
	validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

	# validates :bio,  length: { maximum: 140 }, obscenity: { sanitize: true, replacement: :vowels }
	mount_uploader :avatar, AvatarUploader
	validate :avatar_size

	# Returns the hash digest of the given string.
	def User.digest(string)
  	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                BCrypt::Engine.cost
  	BCrypt::Password.create(string, cost: cost)
	end

	# Returns a random token.
	def User.new_token
  	SecureRandom.urlsafe_base64
	end

	# Remembers a user in the database for use in persistent sessions.
	def remember
  	self.remember_token = User.new_token
  	update_attribute(:remember_digest, User.digest(remember_token))
	end

	# Returns true if the given token matches the digest.
	def authenticated?(attribute, token)
	  digest = send("#{attribute}_digest")
	  return false if digest.nil?
	  BCrypt::Password.new(digest).is_password?(token)
	end

	# Forgets a user.
	def forget
  	update_attribute(:remember_digest, nil)
	end

	#PASSWORD RESET
	def create_reset_digest
  	self.reset_token = User.new_token
  	update_attribute(:reset_digest, User.digest(reset_token))
  	update_attribute(:reset_sent_at, Time.zone.now)
	end

	def send_password_reset_email
  	UserMailer.password_reset(self).deliver_now
	end

	# Returns true if a password reset has expired.
	def password_reset_expired?
  	reset_sent_at < 2.hours.ago
	end
	
	def has_badge?(badge)
		has_badge = false
		self.badges.each do |temp_badge|
			if temp_badge.id == badge.id
				has_badge = true
			end
		end
		return has_badge
	end
	
	def classrooms(attributes="")
		unless attributes.empty?
			return attending_classrooms.select(attributes) + teaching_classrooms.select(attributes)
		else 
			return attending_classrooms + teaching_classrooms
		end
	end
	
  private
	def init
		# Create 3 preset lists when a user signs up.
      self.lists.create(title: "Already Read")
      self.lists.create(title: "Want to Read")
      self.lists.create(title: "Favorites")
	end
		
  	# Validates the size of an uploaded picture.
  	def avatar_size
    	if avatar.size > 5.megabytes
      		errors.add(:avatar, "should be less than 5MB")
    	end
  	end
end
