class Review < ActiveRecord::Base
    default_scope -> { order(created_at: :desc)}
    belongs_to :user
    
    # Obscenity filter
# 	require 'obscenity/active_model'
# 	validates :content,  obscenity: { sanitize: true, replacement: :vowels }
end
