class Classroom < ActiveRecord::Base
	include ClassroomsHelper
	# Associations
	has_many :classroom_teachers
	has_many :classroom_students
	has_many :teachers, through: :classroom_teachers
	has_many :students, through: :classroom_students
	has_many :classcodes
	has_many :terms

	accepts_nested_attributes_for :terms, allow_destroy: true

	# Callbacks
	after_save :set_current_term
	after_save :init
	before_destroy :destroy_children

  # Validations
  validates :title, presence: true, length: { maximum: 50 }

  # Avatar
  mount_uploader :avatar, AvatarUploader
	validate :avatar_size

	# Values
  def has_terms?
  	self.terms.any? ? true : false
  	# self.current_term_id == 0 ? false : true
  end

  def has_current_term?
  	self.current_term_id != 0 ? true : false
  end

	def student_classcode
		return self.classcodes.find_by(relationship_type: "Student")
	end

	def teacher_classcode
		return self.classcodes.find_by(relationship_type: "Teacher")
	end

 #  def current_term_id
 # 	term_id = 0
 # 	if self.has_terms?
	# 		self.terms.each do |term|
	# 			if (term.start_date..term.end_date).include?(Date.today)
	# 				term_id = term.id
	# 			end
	# 		end
	# 	end

	# 	return term_id
	# end

  # Actions

	def set_current_term
		if self.has_terms?
			current_term = false
			self.terms.each do |term|
				if (term.start_date..term.end_date).include?(Date.today)
					update_column :current_term_id, term.id
					current_term = true
				end
			end

			if current_term == false && self.current_term_id != 0
				update_column :current_term_id, 0
			end
		else
		  update_column :current_term_id, 0
		end
	end

	private
		def init
			# Create class codes
			update_column :student_classcode_id, self.classcodes.create(relationship_type: "Student").id
			update_column :teacher_classcode_id, self.classcodes.create(relationship_type: "Teacher").id
		end

		# Validates the size of an uploaded picture.
  	def avatar_size
    	if avatar.size > 5.megabytes
      		errors.add(:avatar, "should be less than 5MB")
    	end
  	end

		def destroy_children
			Classbook.where(classroom_id: self.id).each do |classbook|
				classbook.destroy
			end
			Term.where(classroom_id: self.id).each do |term|
				term.destroy
			end
			Classcode.where(classroom_id: self.id).each do |classcode|
				classcode.destroy
			end
		end
end
