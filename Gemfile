source 'https://rubygems.org'

# Essentials - already here
ruby "2.3.0"
gem 'rails',            '4.2.0'
gem 'rake',             '11.1.1'
gem 'pg',             '0.17.1'
gem 'bootstrap-sass',   '3.2.0.4'
gem 'sass-rails',       '5.0.2'
gem 'uglifier',         '2.5.3'
gem 'coffee-rails',     '4.1.0'
gem 'jquery-rails',     '4.0.3'
gem 'turbolinks',       '5.2.0'
gem 'jquery-turbolinks'
gem 'tzinfo'
gem 'jbuilder',         '2.2.3'
# Added in
gem 'bcrypt' # Secure password token generation
gem 'sdoc',             '0.4.0', group: :doc
gem 'faker',            '1.4.2' # Generating fake users
gem 'vacuum' # Search Amazon
gem 'figaro' # Environment variables
gem 'redcarpet' # Markdown
# gem 'obscenity' # Censor swear words
gem 'nested_form_fields' # for adding fields on the fly in terms
gem 'bootstrap-datepicker-rails'
gem 'will_paginate', '~> 3.1.0'
gem 'bootstrap-will_paginate', '0.0.10'
# AVATARS
gem 'carrierwave',             '0.10.0' # Avatar uploading
gem 'mini_magick',             '3.8.0' # Editing avatars
gem 'fog',                     '1.36.0'
gem 'rollbar',        '2.12.0' # For error messages -> rollbar in production
# -- end avatar pack
gem 'rack'
gem 'resque'
gem 'googlebooks'
# gem 'exception_notification'
gem 'puma_worker_killer'
gem "oink"
gem 'rb-readline'
gem "lazyload-rails"
gem 'rails_admin', '1.3'
# don't upgrade - bundle woudn't install the dependencies needed for 2.2.0
gem "rails_admin_import", "2.1.0"

group :development, :test do
  gem 'byebug',      '3.4.0'
  gem 'web-console', '2.0.0.beta3'
  gem 'spring',      '1.1.3'
  # Rspec stuff
  gem 'rspec'
  gem 'rspec-rails'
  gem 'rspec-core'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'rails_real_favicon' # Still need this?
  gem "better_errors"
  gem "binding_of_caller"
  gem 'bullet', group: 'development'
end

group :test do
  gem 'minitest-reporters', '1.0.5'
  gem 'mini_backtrace',     '0.1.3'
  gem 'guard-minitest',     '2.3.1'
  gem 'capybara'
end

group :production do
  gem 'rails_12factor', '0.0.2'
  gem 'puma',           '2.11.1'
  gem 'skylight'
  gem 'scout_apm'
end
