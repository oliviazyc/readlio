# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190410020101) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "books", force: :cascade do |t|
    t.string   "title"
    t.string   "author"
    t.integer  "list_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.text     "review"
    t.string   "image_url"
    t.string   "asin"
    t.boolean  "manual"
    t.integer  "pages"
    t.string   "amazon_url"
    t.integer  "user_id"
    t.string   "isbn"
    t.text     "editorial_review"
    t.text     "description"
  end

  add_index "books", ["list_id", "created_at"], name: "index_books_on_list_id_and_created_at", using: :btree
  add_index "books", ["list_id"], name: "index_books_on_list_id", using: :btree

  create_table "classbooks", force: :cascade do |t|
    t.string   "title"
    t.string   "author"
    t.text     "review"
    t.string   "image_url"
    t.string   "asin"
    t.boolean  "manual"
    t.integer  "pages"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "user_id"
    t.integer  "classroom_id"
    t.integer  "term_id"
    t.string   "amazon_url"
    t.text     "editorial_review"
    t.string   "isbn"
    t.text     "description"
  end

  add_index "classbooks", ["user_id", "created_at"], name: "index_classbooks_on_user_id_and_created_at", using: :btree
  add_index "classbooks", ["user_id"], name: "index_classbooks_on_user_id", using: :btree

  create_table "classcodes", force: :cascade do |t|
    t.integer  "classroom_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "value"
    t.string   "relationship_type"
  end

  add_index "classcodes", ["classroom_id", "created_at"], name: "index_classcodes_on_classroom_id_and_created_at", using: :btree
  add_index "classcodes", ["classroom_id"], name: "index_classcodes_on_classroom_id", using: :btree

  create_table "classroom_students", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "classroom_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "classroom_students", ["classroom_id"], name: "index_classroom_students_on_classroom_id", using: :btree
  add_index "classroom_students", ["user_id"], name: "index_classroom_students_on_user_id", using: :btree

  create_table "classroom_teachers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "classroom_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "classroom_teachers", ["classroom_id"], name: "index_classroom_teachers_on_classroom_id", using: :btree
  add_index "classroom_teachers", ["user_id"], name: "index_classroom_teachers_on_user_id", using: :btree

  create_table "classrooms", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "classcode"
    t.integer  "required_pages"
    t.integer  "current_term_id"
    t.string   "teacher_classcode"
    t.integer  "student_classcode_id"
    t.integer  "teacher_classcode_id"
    t.boolean  "activity_feed_enabled", default: true,    null: false
    t.boolean  "leaderboards_enabled",  default: true
    t.string   "judge_leaderboards_by", default: "Books"
    t.string   "avatar"
    t.boolean  "archived",              default: false
  end

  add_index "classrooms", ["created_at"], name: "index_classrooms_on_created_at", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "goals", force: :cascade do |t|
    t.string   "title"
    t.integer  "amount"
    t.date     "end_date"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "completed",    default: true, null: false
    t.integer  "start_amount"
  end

  add_index "goals", ["user_id", "created_at"], name: "index_goals_on_user_id_and_created_at", using: :btree
  add_index "goals", ["user_id"], name: "index_goals_on_user_id", using: :btree

  create_table "lists", force: :cascade do |t|
    t.string   "title"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "total_pages"
    t.boolean  "goodreads"
  end

  add_index "lists", ["user_id", "created_at"], name: "index_lists_on_user_id_and_created_at", using: :btree
  add_index "lists", ["user_id"], name: "index_lists_on_user_id", using: :btree

  create_table "reviews", force: :cascade do |t|
    t.string   "asin"
    t.text     "content"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "user_id"
    t.integer  "book_id"
    t.string   "model_type"
    t.boolean  "public",     default: true
  end

  create_table "students", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teachers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "terms", force: :cascade do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.string   "name"
    t.integer  "classroom_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "title"
  end

  add_index "terms", ["classroom_id"], name: "index_terms_on_classroom_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "avatar"
    t.text     "bio"
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "lists_enabled",          default: true
    t.boolean  "points_checked",         default: false
    t.boolean  "profile_private",        default: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
