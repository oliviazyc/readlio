class AddAmazonUrlToClassbooks < ActiveRecord::Migration
  def change
    add_column :classbooks, :amazon_url, :string
  end
end
