class CreateClassbooks < ActiveRecord::Migration
  def change
    create_table :classbooks do |t|
    	t.string   "title"
	    t.string   "author"
	    t.text     "review"
	    t.string   "image_url"
	    t.string   "asin"
	    t.boolean  "manual"
	    t.integer  "pages"
	    t.string   "term"
      	t.timestamps null: false
      	t.references :user, index: true, foreign_key: true
    end
    add_index :classbooks, [:user_id, :created_at]
  end
end
