class AddTypeToClasscodes < ActiveRecord::Migration
  def change
    add_column :classcodes, :type, :string
  end
end
