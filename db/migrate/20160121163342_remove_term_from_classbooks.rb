class RemoveTermFromClassbooks < ActiveRecord::Migration
  def change
    remove_column :classbooks, :term, :integer
  end
end
