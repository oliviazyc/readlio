class AddStartAmountToGoals < ActiveRecord::Migration
  def change
    add_column :goals, :start_amount, :integer
  end
end
