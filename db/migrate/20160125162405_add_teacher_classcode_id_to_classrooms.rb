class AddTeacherClasscodeIdToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :teacher_classcode_id, :integer
  end
end
