class AddPointsCheckedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :points_checked, :boolean, :default => false
  end
end
