class AddStudentClasscodeIdToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :student_classcode_id, :integer
  end
end
