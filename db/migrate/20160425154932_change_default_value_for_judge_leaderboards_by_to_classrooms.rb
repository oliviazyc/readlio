class ChangeDefaultValueForJudgeLeaderboardsByToClassrooms < ActiveRecord::Migration
  def change
    change_column :classrooms, :judge_leaderboards_by, :string, :default => "Books"
  end
end
