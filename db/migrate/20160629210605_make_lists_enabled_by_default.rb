class MakeListsEnabledByDefault < ActiveRecord::Migration
  def change
    change_column :users, :lists_enabled, :boolean, :default => true
  end
end
