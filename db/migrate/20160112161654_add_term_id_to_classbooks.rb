class AddTermIdToClassbooks < ActiveRecord::Migration
  def change
    add_column :classbooks, :term_id, :integer
  end
end
