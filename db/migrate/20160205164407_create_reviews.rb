class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :asin
      t.text :content
      t.string :model_type
      
      t.references :book, index: true, foreign_key: true
      t.references :classbook, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      
      t.timestamps null: false
    end
  end
end
