class CreateTableClassroomUsers < ActiveRecord::Migration
  def change
    create_table :classrooms do |t|
    	t.string :title
    	t.integer :current_term_id
    	t.timestamps null: false
    end
    add_index :classrooms, [:created_at]
  end
end