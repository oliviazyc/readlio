class AddProfilePrivateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :profile_private, :boolean, :default => false
  end
end
