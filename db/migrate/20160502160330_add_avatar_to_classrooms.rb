class AddAvatarToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :avatar, :string
  end
end
