class AddListsEnabledToUsers < ActiveRecord::Migration
  def change
    add_column :users, :lists_enabled, :boolean
  end
end
