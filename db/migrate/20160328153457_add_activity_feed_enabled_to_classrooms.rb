class AddActivityFeedEnabledToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :activity_feed_enabled, :boolean, null: false, default: true
  end
end
