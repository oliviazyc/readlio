class AddTotalPagesToLists < ActiveRecord::Migration
  def change
    add_column :lists, :total_pages, :integer
  end
end
