class ChangeLeaderboardsEnabledToBeTrueByDefaultToClassrooms < ActiveRecord::Migration
  def change
    change_column :classrooms, :leaderboards_enabled, :boolean, :default => true
  end
end
