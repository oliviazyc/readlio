class AddClasscodeToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :classcode, :string
  end
end
