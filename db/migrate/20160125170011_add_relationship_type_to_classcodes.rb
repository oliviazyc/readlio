class AddRelationshipTypeToClasscodes < ActiveRecord::Migration
  def change
    add_column :classcodes, :relationship_type, :string
  end
end
