class AddEditorialReviewToClassbooks < ActiveRecord::Migration
  def change
    add_column :classbooks, :editorial_review, :text
  end
end
