class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.references :list, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :books, [:list_id, :created_at]
  end
end
