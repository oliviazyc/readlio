class ChangeIsbnToString < ActiveRecord::Migration
  def change
  	change_column :books, :isbn, :string
  	change_column :classbooks, :isbn, :string

  end
end
