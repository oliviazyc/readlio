class AddClassroomIdToClassbooks < ActiveRecord::Migration
  def change
    add_column :classbooks, :classroom_id, :integer
  end
end
