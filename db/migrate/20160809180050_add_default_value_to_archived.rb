class AddDefaultValueToArchived < ActiveRecord::Migration
  def change
    change_column :classrooms, :archived, :boolean, :default => false
  end
end
