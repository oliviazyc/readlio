class AddRequiredPagesToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :required_pages, :integer
  end
end
