class AddManualToBooks < ActiveRecord::Migration
  def change
    add_column :books, :manual, :boolean
  end
end
