class AddValueToClasscodes < ActiveRecord::Migration
  def change
    add_column :classcodes, :value, :string
  end
end
