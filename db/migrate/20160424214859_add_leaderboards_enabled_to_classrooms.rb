class AddLeaderboardsEnabledToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :leaderboards_enabled, :boolean
    add_column :classrooms, :judge_leaderboards_by, :string
  end
end
