class AddDefaultValueForActivityFeedEnabledToClassrooms < ActiveRecord::Migration
  def change
    change_column :classrooms, :activity_feed_enabled, :boolean, default: true
  end
end
