class AddGoodreadsToLists < ActiveRecord::Migration
  def change
    add_column :lists, :goodreads, :boolean
  end
end
