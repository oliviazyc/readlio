class RemoveTypeFromClasscodes < ActiveRecord::Migration
  def change
    remove_column :classcodes, :type, :string
  end
end
