class AddArchivedToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :archived, :boolean
  end
end
