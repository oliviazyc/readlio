class AddIsbnToClassbooks < ActiveRecord::Migration
  def change
    add_column :classbooks, :isbn, :float
  end
end
