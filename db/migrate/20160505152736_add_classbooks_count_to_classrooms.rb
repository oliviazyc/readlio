class AddClassbooksCountToClassrooms < ActiveRecord::Migration
  def change
    add_column :classrooms, :classbooks_count, :integer
  end
end
