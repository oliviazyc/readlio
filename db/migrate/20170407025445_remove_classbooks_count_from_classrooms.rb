class RemoveClassbooksCountFromClassrooms < ActiveRecord::Migration
  def change
  	remove_column :classrooms, :classbooks_count
  end
end
