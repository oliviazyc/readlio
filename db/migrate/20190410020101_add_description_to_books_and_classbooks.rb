class AddDescriptionToBooksAndClassbooks < ActiveRecord::Migration
  def change
    add_column :books, :description, :text
    add_column :classbooks, :description, :text
  end
end
