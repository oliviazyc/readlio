hp = User.create!(name:  "Harry Potter",
           email: "hp@readlio.com",
           password:              "foobar",
           password_confirmation: "foobar")

pj = User.create!(name:  "Percy Jackson",
           email: "pj@readlio.com",
           password:              "foobar",
           password_confirmation: "foobar")
           
ls = User.create!(name:  "Luke Skywalker",
           email: "ls@readlio.com",
           password:              "foobar",
           password_confirmation: "foobar")
           
example_classroom = hp.teaching_classrooms.create!(title:"Example Class") # Unless you can generate classcodes, it's not going to work
example_classroom.students << [ls, pj]

# Create classcodes
example_classroom.classcodes.create(relationship_type: "Student")
example_classroom.student_classcode_id = Classcode.last.id
example_classroom.classcodes.create(relationship_type: "Teacher")
example_classroom.teacher_classcode_id = Classcode.last.id
example_classroom.save

20.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@readlio.com"
  password = "foobar"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
  example_classroom.students << User.last
end

# t1 = Term.create!(title: "Example Term 1", classroom_id: example_classroom.id, start_date: "2016-8-1", end_date: "2016-8-31")
# t2 = Term.create!(title: "Example Term 2", classroom_id: example_classroom.id, start_date: "2016-9-1", end_date: "2016-9-31")

for i in 0..2 # For the term id to be assigned, you need to disable 
  pj.classbooks.create!(title:"No Term #{i+1}", author: "Example", pages: "100", manual: true, classroom_id: example_classroom.id, term_id: 0, review: "")
  # ls.classbooks.create!(title:"T1 #{i+4}", author: "Example", pages: "100", manual: true, classroom_id: example_classroom.id, term_id: 1, review: "")
  # ls.classbooks.create!(title:"T2 #{i+7}", author: "Example", pages: "100", manual: true, classroom_id: example_classroom.id, term_id: 2, review: "") 
end

# Create an archived classroom
archived_classroom = hp.teaching_classrooms.create!(title:"Archived Classroom", archived: true)
archived_classroom.students << [ls, pj]