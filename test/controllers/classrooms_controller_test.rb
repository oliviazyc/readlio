require 'test_helper'

class ClassroomsControllerTest < ActionController::TestCase
	def setup
		@student_one = users(:student_one)
		@student_two = users(:student_two)
		@teacher = users(:olivia)
		
		@classroom = classrooms(:one)

		@classroom.teachers << @teacher
		@classroom.students << @student_one
		@classroom.students << @student_two
	end

  test "should alert when id is fake" do
  	log_in_as @student_one
  	assert_raises(ActionController::UrlGenerationError) do
			get remove_student_path(:id => "")
		end
  end
  
  test "should redirect if not logged in" do
  	# Try to go to new when not logged in
  	get new_classroom_path
  	assert_redirected_to login_path
  	assert_not flash.nil?
  	# Try to create a classroom when not logged in
  	get new_classroom_path
  	assert_no_difference 'Classroom.count' do
	    post classrooms_path, classroom: { title: "Example" }
    end
    assert_redirected_to login_path
    assert_not flash.nil?
    # Try to get @classroom's show page when not logged in
    get classroom_path(@classroom)
    assert_redirected_to login_path
    assert_not flash.nil?
    # Try to go to classroom index when not logged in
    get classrooms_path(@user)
    assert_redirected_to login_path
    assert_not flash.nil?
  end

  test "should redirect not logged in users from trying to remove other students" do
  	# assert_no_difference 'ClassroomStudent.count'
  end
	
end
