require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  
  def setup
    @base_title = "Readlio"
  end

  test "should get front" do
    get :front
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "should get support" do
    get :support
    assert_response :success
    assert_select "title", "Support | #{@base_title}"
  end

  # test "should get about" do
  #   get :about
  #   assert_response :success
  #   assert_select "title", "About | #{@base_title}"
  # end

end
