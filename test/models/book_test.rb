require 'test_helper'

class BookTest < ActiveSupport::TestCase
  def setup
  	@book = books(:harry_potter)
  end

  # test "should be valid" do
  # 	assert @book.valid?
  # end

  test "list id should be present in a book instance" do
  	@book.list_id = nil
  	assert_not @book.valid?
  end

  test "book title should be present" do
  	@book.title = " "
  	assert_not @book.valid?
  end

end
