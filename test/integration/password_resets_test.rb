require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest

  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:olivia)
  end

  test "password resets" do
    get new_password_reset_path
    assert_template 'password_resets/new'
    assert_select "a[href=?]", login_path
    # Invalid email
    post password_resets_path, password_reset: { email: "" }
    assert_not flash.nil?
    assert_template 'password_resets/new'
    # Valid email
    post password_resets_path, password_reset: { email: @user.email }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest
    assert_equal 1, ActionMailer::Base.deliveries.size
    assert_not flash.nil?
    assert_redirected_to login_path
    # Password reset form
    user = @user
    # Wrong email
    get edit_password_reset_path(user.reset_token, email: "", id: user.id)
    assert_redirected_to new_password_reset_path
    # Right email, wrong token
    get edit_password_reset_path('wrong token', email: user.email, id: user.id)
    assert_response :success
    # Right email, right token
    get edit_password_reset_path(user.reset_token, email: user.email, id: user.id)
    assert_template 'password_resets/edit'
    assert_select "input[name=email][type=hidden][value=?]", user.email
    # Invalid password & confirmation
    patch password_reset_path(user.reset_token, id: user.id),
          email: user.email,
          user: { password:              "foobaz",
                  password_confirmation: "barquux" }

    assert_select 'div#error_explanation'
    # Empty password
    patch password_reset_path(user.reset_token, id: user.id),
          email: user.email,
          user: { password:              "",
                  password_confirmation: "" }
    assert_select 'div#error_explanation' # BUG: There should be an error, but there isn't.
    # Valid password & confirmation
    patch password_reset_path(user.reset_token, id: user.id),
          email: user.email,
          user: { password:              "foobaz",
                  password_confirmation: "foobaz" }
    assert is_logged_in?
    assert_not flash.nil?
    assert_redirected_to user
  end
end