require 'test_helper'

class ClassroomsTest < ActionDispatch::IntegrationTest
	def setup
		@classroom = classrooms(:one)
		@user = users(:olivia)
	end

	test "create new classroom with invalid information" do
		log_in_as @user
		get new_classroom_path
		assert_template 'classrooms/new'
		assert_no_difference 'Classroom.count' do
	    post classrooms_path, classroom: { title:  "" }
    end
    assert_template 'classrooms/new'
    assert_select 'div#error_explanation' # BUG: Why isn't there one?
 	end

  test "create new classroom with valid information" do
  	log_in_as @user
		get new_classroom_path
		assert_template 'classrooms/new'
		assert_difference 'Classroom.count', 1 do
      post classrooms_path, classroom: { title:  "Example" }
    end
    follow_redirect!
    assert_template 'classrooms/show'
    assert_select 'div#error_explanation', false, "This page contains no error explanations."
    assert_equal( @classroom.activity_feed_enabled, true, "Activity feed is not by default true." )
  end

  # test "ensure that the correct text shows when there are no terms" do
  # 	log_in_as @user
  # 	post_via_redirect classrooms_path, classroom: { classrooms(:one) }

		# get classroom_path(@classroom)
		# assert_template 'classrooms/show'
		# assert_select "span", {count: 0, text: "Current Term"}, "This page must contain no spans that say Current Term"
  # end
  
  test "edit classroom with invalid information" do
    log_in_as @user
    post classrooms_path, classroom: @classroom
  	get edit_classroom_path(@classroom)
  	assert_redirected_to edit_classroom_path(@classroom)
  	patch classroom_path(@classroom), classroom: { title:  "" }
  	# assert_template 'classrooms/edit'
    assert_select 'div#error_explanation' # Don't know why it doesn't work - works when tested manually.
  end
  
  test "edit classroom with valid information" do
  	get edit_classroom_path(@classroom)
  	log_in_as @user
  	assert_redirected_to edit_classroom_path(@classroom)
  	patch classroom_path(@classroom), classroom: { title:  "Example" }
  	# assert_template 'classrooms/edit'
    assert_select 'div#error_explanation', false
  end
end