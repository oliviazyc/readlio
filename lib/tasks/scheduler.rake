desc "This task is called by the Heroku scheduler add-on"
task :set_current_term => :environment do
    Classroom.all.each do |classroom|
        classroom.set_current_term
    end
end
