require 'rails_helper'

feature "user visits homepage" do
	def setup
		@classroom = classrooms(:one)
		@user = users(:olivia)
	end

	scenario "create new classroom with invalid information" do
		log_in_as @user
		get new_classroom_path
		assert_template 'classrooms/new'
		assert_no_difference 'Classroom.count' do
      post classrooms_path, classroom: { title:  "" }
    end
    assert_template 'classrooms/new'
    assert_select 'div#error_explanation'
  end
end