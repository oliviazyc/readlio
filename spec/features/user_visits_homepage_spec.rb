require 'rails_helper.rb'
feature "user visits homepage" do
	# The user is logged out so it goes to staticpages#front
    scenario "successfully" do
    	visit root_path
    	expect(page).to have_css "h3", text: "Incredibly easy to use."
    end
    # The user is logged in so it doesn't go to staticpages#front
    scenario "unsuccessfully" do
    	visit root_path
    	expect(page).not_to have_css "h1", text: "Your classrooms"
    end
end