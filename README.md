== README

Readlio
-------

Readlio is an easy way to keep track of all your books. With Readlio, you can find books to read next, share your book log with friends or teachers, or write reviews.

**Complaints or suggestions? Email readlioinc@gmail.com.**

Got a bug to report? Please submit the issue to the repository issues.